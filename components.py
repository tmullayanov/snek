'''
This file declares base components
that are needed to build "engine" for game
'''
import abc


class InputComp(abc.ABC):
    def __init__(self, base):
        self.base = base

    @abc.abstractmethod
    def input(self, *args, **kwargs):
        pass


class GfxComp(abc.ABC):
    def __init__(self, base, scene):
        self.base = base
        self.scene = scene
        self.attachToScene()

    @abc.abstractmethod
    def draw(self):
        pass

    def attachToScene(self):
        self.scene.attach(self)

    def detach(self):
        self.scene.detach(self)


class PhysComp(abc.ABC):
    def __init__(self, base):
        self.base = base

    @abc.abstractmethod
    def interact(self):
        pass