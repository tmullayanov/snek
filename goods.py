'''
This file defines FOOD supplies.
'''
import components
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

class Goods:

    class FoodPhys(components.PhysComp):
        def __init__(self, base, world):
            super().__init__(base)
            self.base = base
            self.world = world
            self.world.addObject(self)

        def interact(self):
            pass

        def contains(self, point):
            return point in self.base.points

        def getEmptyCell(self):
            return self.getNEmptyCells(1)

        def getNEmptyCells(self, N):
            return self.world.getEmptyCells(N)

        def __len__(self):
            return len(self.base.points)

    class FoodGLGfx(components.GfxComp):
        def __init__(self, base, scene):
            super().__init__(base, scene)

        def draw(self):
            super().draw()
            scale = self.base.scale  # hack
            glColor3f(1.0, 1.0, 0.0)
            for (x, y) in self.base.points:
                glRectf(x * scale, y * scale, (x + 1) * scale, (y + 1) * scale)
            glFlush()

    def __init__(self, world, scene, scale, N=10, gfxBackend='gl'):
        self._maxSupplies = N
        self.scale = scale
        self.points = []

        self.phys = self.FoodPhys(self, world)
        if gfxBackend == 'gl':  # hack. Re-write it with sane dispatching method
            self.gfx = self.FoodGLGfx(self, scene)

        _points = self.phys.getNEmptyCells(N)
        self.points = _points if _points else []

    def __len__(self):
        return len(self.points)

    def pointEaten(self, point):
        idx = self.points.index(point)
        repl = self.phys.getEmptyCell()
        if repl:
            self.points[idx] = repl[0]
        else:
            del self.points[idx]
