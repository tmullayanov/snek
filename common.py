'''
This file defines functionality that should belong to several modules.
'''


class GameOver(ValueError): pass


def apply_on_all(seq, method, *args, **kwargs):
    for obj in seq:
        getattr(obj, method)(*args, **kwargs)