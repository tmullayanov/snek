'''
This file defines SNAKE class and all it's internal entities.
'''
import components
from common import GameOver
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from world import WorldMessages
import goods


class SnakeGLGFX(components.GfxComp):
    def __init__(self, base, scene):
        super().__init__(base, scene)

    def draw(self):
        super().draw()
        scale = self.base.scale # hack
        glColor3f(0.0, 1.0, 1.0)
        for (x, y) in self.base.body:
            glRectf(x*scale, y*scale, (x+1)*scale, (y+1)*scale)
        glFlush()


class SnakeNullGFX(components.GfxComp):
    pass


class SnakePhys(components.PhysComp):
    def __init__(self, base, world):
        self.base = base
        self.world = world
        world.addObject(self)

    def interact(self):
        super().interact()
        self.base.move()

    def contains(self, point):
        return self.base.contains(point)

    def __len__(self):
        return len(self.base)


class SnakeGLInput(components.InputComp):
    def __init__(self, base):
        self.base = base
        self.map = {GLUT_KEY_LEFT:   (-1, 0),
                    GLUT_KEY_RIGHT:  (1, 0),
                    GLUT_KEY_UP:     (0, 1),
                    GLUT_KEY_DOWN:   (0, -1)}

    def locked(self, dir):
        baseDir = self.base.direction
        return not any(a+b for (a, b) in zip(baseDir, dir))

    def input(self, key, x, y, *args, **kwargs):
        super().input(*args, **kwargs)
        dir = self.map[key]
        if self.locked(dir):
            return
        self.base.direction = dir


class Snake():
    BASE_LENGTH = 3

    def __init__(self, scene, head, scale, world, gfxBackend='gl'): #HACK
        self.length = Snake.BASE_LENGTH
        self.scale = scale
        x, y = head

        self.body=[head]
        self.Gfx = SnakeGLGFX(self, scene)
        self.input = SnakeGLInput(self)
        self.phys = SnakePhys(self, world)
        self.direction = (0, 0)
        self.score = 0

    def __len__(self):
        return self.length

    def move(self):
        if self.direction == (0, 0):
            return

        x, y = self.body[0]
        dx, dy = self.direction
        newHead = (x+dx, y+dy)
        cell = self.phys.world.check_cell(*newHead)
        print('moveTo: {} type: {}'.format(newHead, cell))

        self.body[1:] = self.body[:-1]
        self.__handle_cell__(type(cell), cell, newHead)
        self.body[0] = newHead

    def contains(self, point):
        return point in self.body

    def __break__(self, point):
        idx = self.body.index(point)
        self.score -= self.length - idx
        self.length = idx
        self.body = self.body[:idx]

    def __handle_cell__(self, _type, obj, point):
        if obj == WorldMessages.outside:
            raise GameOver("Outside of the field!", "score={}".format(self.score))
        elif obj == WorldMessages.empty:
            return
        elif type(obj) == Snake:
            obj.__break__(point)
        elif type(obj) == goods.Goods:
            obj.pointEaten(point)
            self.body = [point] + self.body
            self.score += 1