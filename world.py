'''
This file defines WORLD base class.
In short, the WORLD class acts like a container of PhysComp's.
The instance of the class is responsible for updating all components accordingly.
'''
import abc
import enum
from components import PhysComp
import common
import random


class WorldMessages(enum.Enum):
    outside = 1
    empty = 2
    custom = 3


class World(abc.ABC):
    objects = []

    def __init__(self, w, h):
        self.width = w
        self.height = h

    def addObject(self, obj):
        self.objects.append(obj)

    def removeObject(self, obj):
        self.objects.remove(obj)

    def evolve(self):
        common.apply_on_all(self.objects, 'interact')

    def __in_field_p(self, x, y):
        return (0 <= x < self.width) and (0 <= y < self.height)

    def check_cell(self, x, y):
        ''' Returns the type of object located at (x, y). Never raises exception. '''
        if not self.__in_field_p(x, y):
            return WorldMessages.outside
        for obj in self.objects:
            if obj.contains((x, y)):
                return obj.base
        return WorldMessages.empty

    def cellsLeft(self):
        return self.width * self.height - sum(len(obj) for obj in self.objects)

    def cellsTaken(self):
        return sum(len(obj) for obj in self.objects)

    def getEmptyCells(self, N):
        print('N={}'.format(N))
        _N = min(N, self.cellsLeft())
        if 0 < _N < N:
            print('warning:: only {} cells would be generated instead of {}'.format(_N, N))
        elif _N == 0:
            return
        points = set()
        while len(points) < _N:
            x = random.randint(0, self.width-1)
            y = random.randint(0, self.height-1)
            if self.check_cell(x, y) == WorldMessages.empty:
                points.add((x, y))
        return list(points)
