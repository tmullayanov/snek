from engine import Engine
from OpenGLEngine import GLEngine
from components import *
import world
import scene, gridscene
from snake import Snake
from goods import Goods

if __name__ == '__main__':
    w,h, scale = 20, 30, 15
    scene = gridscene.GLGridScene(w, h, scale)
    basicWorld = world.World(w, h)
    snake = Snake(scene, (1, 1), scale, basicWorld)

    supplies = Goods(basicWorld, scene, scale)

    engine = GLEngine(basicWorld, scene)
    engine.registerInput(snake.input.input) # hack
    engine.run()