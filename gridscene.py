'''
This file defines GridScene class which represents main Scene with grid, Snake and fruits.
'''
import scene
import components
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *

class GLGridScene(scene.Scene):

    def __init__(self, w, h, scale):
        super().__init__()
        assert (w > 0 and h > 0), "Negative scene size is not allowed!"
        self.w, self.h = w, h
        self.scale = scale

    def drawGrid(self):
        glColor3f(0.0, 1.0, 0.0)
        glBegin(GL_LINES)
        for i in range(0, self.w+1):
            glVertex2f(i*self.scale, 0)
            glVertex2f(i*self.scale, self.scale*self.h)

        for i in range(0, self.h+1):
            glVertex2f(0, i*self.scale)
            glVertex2f(self.scale*self.w, i*self.scale)

        glEnd()
        glFlush()

    def draw(self):
        self.drawGrid()
        super().draw()