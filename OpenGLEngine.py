import engine
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *


class GLEngine(engine.Engine):

    def __init__(self, world, scene):
        super().__init__(world, scene)
        self.__setup__()

    def __setup__(self):
        glutInit()
        glutInitDisplayMode(GLUT_RGB)
        glutInitWindowSize(800, 600)
        glutCreateWindow("Snek")
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluOrtho2D(-20, 800, -20, 600) # TODO: get rid of those magic numbers!
        glutDisplayFunc(self.display)
        glutTimerFunc(100, self.step, 0)
        glClearColor(0.0,0.0,0.0,0.0)

    def registerInput(self, handler):
        glutSpecialFunc(handler)

    def evolve(self):
        super().evolve()

    def display(self):
        glClear(GL_COLOR_BUFFER_BIT)
        super().display()

    def step(self, arg):
        super().step()
        glutTimerFunc(100, self.step, 0)

    def run(self):
        glutMainLoop()

