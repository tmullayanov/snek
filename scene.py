'''
This file defines SCENE base class.
SCENE acts as a container of GfxComps and draws them all sequentially
'''
import abc
from components import GfxComp
import common


class Scene(abc.ABC):
    def __init__(self):
        self.components = []

    def attach(self, component):
        assert isinstance(component, GfxComp)
        self.components.append(component)

    def detach(self, component):
        self.components.remove(component)

    @abc.abstractmethod
    def draw(self):
        common.apply_on_all(self.components, 'draw')