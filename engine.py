'''
SNEK project.
This file defines ENGINE class which holds the WORLD, SCENE and proceeds game state step by step.
'''
import abc


class Engine(abc.ABC):
    def __init__(self, world, scene):
        self.world = world
        self.scene = scene

    @abc.abstractmethod
    def display(self):
        self.scene.draw()

    @abc.abstractmethod
    def evolve(self):
        self.world.evolve()

    @abc.abstractmethod
    def step(self):
        self.display()
        self.evolve()

    @abc.abstractmethod
    def run(self):
        pass