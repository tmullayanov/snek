# SNEK #

This is SNEK - another Snake game remaster.

### Why do we need another Snake? ###

This project was started for two reasons:

* Revise my design skills
* Build a platform for bot-building pratice (self-learning bots included)

### Why *SNEK*? ###

[That's](http://knowyourmeme.com/memes/this-is-snek) why. 

I just couldn't resist temptation to name it like that.